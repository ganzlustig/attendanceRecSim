package ganzlustig.attendancerecsim

import kotlinx.serialization.Serializable
import java.time.DayOfWeek
import java.time.Duration
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.temporal.ChronoUnit

@Serializable
data class WeekDayTime(val day: DayOfWeek, @Serializable(with=LocalTimeSerializer::class) val time: LocalTime) {

    fun minus(other: WeekDayTime): Duration {

        val days = (day.value - other.day.value) % 7L
        return Duration.between(other.time, time).plus(Duration.ofDays(days))
    }

    fun plus(duration: Duration): WeekDayTime {

        val newDay = DayOfWeek.of(((day.value + duration.toDays()) % 7).toInt())
        val newTime = time.plus(duration)
        return WeekDayTime(newDay, newTime)
    }

    fun isAfter(other: WeekDayTime): Boolean {
        return (other.day > day) || ((other.day == day) && (other.time > time))
    }

    fun nextAfter(reference: LocalDateTime): LocalDateTime {

        return if (reference.dayOfWeek == day) {
            if (reference.toLocalTime() <= time) {
                reference.plus(7, ChronoUnit.DAYS).with(time)
            } else {
                reference.with(time)
            }
        } else {
            reference.plus((day.value - reference.dayOfWeek.value + 7).toLong() % 7, ChronoUnit.DAYS).with(time)
        }
    }

    fun serialize() : List<String> {
        return listOf(day.name, time.toString())
    }

    companion object {
        fun parse(parts: List<String>) : WeekDayTime {
            return WeekDayTime(DayOfWeek.valueOf(parts[0].toUpperCase()), LocalTime.parse(parts[1]))
        }
    }
}

fun LocalDateTime.toWeekDayTime() : WeekDayTime {
    return WeekDayTime(dayOfWeek, toLocalTime())
}
