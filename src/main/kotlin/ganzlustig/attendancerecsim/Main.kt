package ganzlustig.attendancerecsim

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.mainBody

/**
 * Read a CSV table, and convert each row into it's own OKH YAML manifest file.
 */
fun main(args: Array<String>) = mainBody {

    ArgParser(args).parseInto(::ParsedArgs).run {

//        checkNotNull(workerParams) { "Please provide a workers parameters file with -w or --workers" }

        if (writeSampleWorkers != null) {
            log.info("Generating sample workers file to '{}' ...", writeSampleWorkers)
            if (writeSampleWorkers!!.extension.toLowerCase() == "csv") {
                writeWorkersCsv(writeSampleWorkers!!, Worker.createSamples())
            } else {
                writeWorkersYaml(writeSampleWorkers!!, Worker.createSamples())
            }
            log.info("done.")
        } else {
            if (workerParams == null) {
                throw IllegalStateException("Please provide either:\n" +
                        "* a workers parameters file with -w or --workers, or\n" +
                        "* a sample workers file location to be generated with --sample-workers")
            }

            val workers =
                if (workerParams!!.extension.toLowerCase() == "csv") {
                    readWorkersCsv(workerParams!!)
                } else {
                    readWorkersYaml(workerParams!!)
                }

            val sim = Simulation(workers, perfectEnding, durationFromHours(workDayHours), seed)
            val simRes = sim.call()

            if (!noICalOutput) {
                writeICal(simRes, iCalOutput)
            }
            writeWorkShiftsCsv(simRes, csvOutputWorkShifts)
            writeDayStatesCsv(simRes, csvOutputDayStates)
        }
    }
}
