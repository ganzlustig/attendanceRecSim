package ganzlustig.attendancerecsim

import kotlinx.serialization.Serializable
import java.lang.RuntimeException
import java.lang.reflect.Modifier
import java.time.DayOfWeek
import java.time.LocalDate
import kotlin.math.ceil

/**
 * @param firstName The personal first name.
 * @param lastName The personal last name.
 * @param fullTimeFraction How big a fraction of a full-time employment
 *        the worker is supposed to me working.
 *        For example, 0.5 means, a 50% employment.
 * @param sicknessFraction How big a fraction of life-time
 *        the worker is usually sick.
 *        For example, 0.03 means, he is sick 3% of the time.
 * @param steadinessFraction How much the worker generally sticks to his core-times.
 *        1.0 means, all the time, 0.5 means quite little,
 *        and 0.0 would signify totally erratic times.
 * @param weeklyCoreTimes Specifies which times on which days of a common work-week
 *        the person is most likely to be working
 * @param employmentTimes During which dates the person was employed.
 * @param yearlyVacationDays days of payed vacation a year;
 *        absolute, not relative to fullTimeFraction
 * @param germanStateOfEmployment two letter abbreviation, like "BE" for Berlin;
 *        this is relevant for figuring out the holidays
 */
@Serializable
data class Worker(
    val firstName: String,
    val lastName: String,
    val email: String,
    val fullTimeFraction: Double,
    val sicknessFraction: Double,
    val steadinessFraction: Double,
    val weeklyCoreTimes: List<WorkShiftPlan>,
    val employmentTimes: List<DatePeriod>,
    val yearlyVacationDays: Int,
    val germanStateOfEmployment: String = DEFAULT_GERMAN_STATE)
{
    fun serialize() : List<String> {
        return listOf(
            firstName,
            lastName,
            email,
            fullTimeFraction.toString(),
            sicknessFraction.toString(),
            steadinessFraction.toString(),
            weeklyCoreTimes.map { it.serialize().foldWithDelim("|") }.foldWithDelim(";"),
            employmentTimes.map { it.serialize().foldWithDelim("|") }.foldWithDelim(";"),
            yearlyVacationDays.toString(),
            germanStateOfEmployment)
    }

    companion object {
        const val DEFAULT_FULL_TIME_FRACTION = 1.0
        const val DEFAULT_SICKNESS_FRACTION = 0.03
        const val DEFAULT_STEADINESS_FRACTION = 0.2
        const val DEFAULT_FULL_TIME_YEARLY_VACATION_DAYS = 4 * 5
        const val DEFAULT_GERMAN_STATE = "BE"

        fun parseHeader(header: List<String>) : Map<String, Int> {

            val titleToIndex = mutableMapOf<String, Int>()
            val workerFields = Worker::class.java.declaredFields
            for (field in workerFields) {
                if (Modifier.isStatic(field.modifiers)) {
                    continue
                }
                val fieldIndex = header.indexOf(field.name)
                if (fieldIndex >= 0) {
                    titleToIndex[field.name] = fieldIndex
                }
            }

            return titleToIndex
        }

        fun createDefaultHeader(maxSize: Int) : Map<String, Int> {

            val titleToIndex = mutableMapOf<String, Int>()
            val workerFields = Worker::class.java.declaredFields
            var index = 0
            for (field in workerFields) {
                if (Modifier.isStatic(field.modifiers)) {
                    continue
                }
                titleToIndex[field.name] = index++
                if (index >= maxSize) {
                    break
                }
            }

            return titleToIndex
        }

        private fun extract(row: List<String>, titleToIndex: Map<String, Int>, field: String, default: String) : String {

            val index = titleToIndex[field] ?: return default
            return row[index]
        }

        private fun parseWeeklyCoreTimes(str: String) : List<WorkShiftPlan> {

            return str
                .splitWithSpace(";")
                .filter {
                    it.isNotEmpty()
                }
                .map {
                    WorkShiftPlan.parse(it.splitWithSpace("|"))
                }
        }

        private fun parseEmploymentTimes(str: String) : List<DatePeriod> {

            val employmentTimes = str
                .splitWithSpace(";")
                .filter {
                    it.isNotEmpty()
                }
                .map {
                    DatePeriod.parse(it.splitWithSpace("|"))
                }
            if (employmentTimes.isEmpty()) {
                throw RuntimeException("Failed to parse employmentTimes from: '${str}'")
            }
            return employmentTimes
        }

        private fun defaultVacationDays(fullTimeFraction: Double) : Int {

            return ceil(fullTimeFraction * DEFAULT_FULL_TIME_YEARLY_VACATION_DAYS).toInt()
        }

        private fun vacationDays(days: String, fullTimeFraction: Double) : Int {

            return if (days.isEmpty()) {
                defaultVacationDays(fullTimeFraction)
            } else {
                days.toInt()
            }
        }

        fun parse(row: List<String>, titleToIndex: Map<String, Int>) : Worker {

            val firstName = extract(row, titleToIndex, Worker::firstName.name, "")
            val lastName = extract(row, titleToIndex, Worker::lastName.name, "")
            val email = extract(row, titleToIndex, Worker::email.name, "${firstName}.${lastName}@something.com")
            val fullTimeFraction = toDoubleLax(extract(row, titleToIndex, Worker::fullTimeFraction.name, DEFAULT_FULL_TIME_FRACTION.toString()))
            val sicknessFraction = toDoubleLax(extract(row, titleToIndex, Worker::sicknessFraction.name, DEFAULT_SICKNESS_FRACTION.toString()))
            val steadinessFraction = toDoubleLax(extract(row, titleToIndex, Worker::steadinessFraction.name, DEFAULT_STEADINESS_FRACTION.toString()))
            val weeklyCoreTimes = parseWeeklyCoreTimes(extract(row, titleToIndex, Worker::weeklyCoreTimes.name, ""))
            val employmentTimes = parseEmploymentTimes(extract(row, titleToIndex, Worker::employmentTimes.name, "")) // 2010-01-01|2020-12-31
            val yearlyVacationDays = vacationDays(extract(row, titleToIndex, Worker::yearlyVacationDays.name, ""), fullTimeFraction)
            val germanStateOfEmployment = extract(row, titleToIndex, Worker::germanStateOfEmployment.name, DEFAULT_GERMAN_STATE)

            return Worker(
                firstName,
                lastName,
                email,
                fullTimeFraction,
                sicknessFraction,
                steadinessFraction,
                weeklyCoreTimes,
                employmentTimes,
                yearlyVacationDays,
                germanStateOfEmployment)
        }

        fun createSamples() : List<Worker> {

            val jane = Worker(
                "Jane",
                "Doe",
                "jane.doe@email.com",
                1.0,
                0.04,
                0.3,
                listOf(
                    WorkShiftPlan(DayOfWeek.MONDAY, 8, 0, 4),
                    WorkShiftPlan(DayOfWeek.MONDAY, 13, 0, 5),
                    WorkShiftPlan(DayOfWeek.TUESDAY, 8, 0, 4),
                    WorkShiftPlan(DayOfWeek.TUESDAY, 13, 0, 5),
                    WorkShiftPlan(DayOfWeek.WEDNESDAY, 8, 0, 4),
                    WorkShiftPlan(DayOfWeek.WEDNESDAY, 13, 0, 5),
                    WorkShiftPlan(DayOfWeek.THURSDAY, 8, 0, 4),
                    WorkShiftPlan(DayOfWeek.THURSDAY, 13, 0, 5),
                    WorkShiftPlan(DayOfWeek.FRIDAY, 8, 0, 4),
                    WorkShiftPlan(DayOfWeek.FRIDAY, 13, 0, 2)
                ),
                listOf(DatePeriod(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 12, 31))),
                defaultVacationDays(1.0)
            )

            val john = Worker(
                "John",
                "Dao",
                "john.dao@email.com",
                0.5,
                0.02,
                0.1,
                listOf(
                    WorkShiftPlan(DayOfWeek.MONDAY, 8, 0, 4),
                    WorkShiftPlan(DayOfWeek.MONDAY, 13, 0, 5),
                    WorkShiftPlan(DayOfWeek.TUESDAY, 8, 0, 4),
                    WorkShiftPlan(DayOfWeek.TUESDAY, 13, 0, 5),
                    WorkShiftPlan(DayOfWeek.WEDNESDAY, 8, 0, 4),
                    WorkShiftPlan(DayOfWeek.WEDNESDAY, 13, 0, 5),
                    WorkShiftPlan(DayOfWeek.THURSDAY, 8, 0, 4),
                    WorkShiftPlan(DayOfWeek.THURSDAY, 13, 0, 5),
                    WorkShiftPlan(DayOfWeek.FRIDAY, 8, 0, 4),
                    WorkShiftPlan(DayOfWeek.FRIDAY, 13, 0, 2)
                ),
                listOf(DatePeriod(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 12, 31))),
                defaultVacationDays(0.5)
            )

            return listOf(jane, john)
        }
    }
}
