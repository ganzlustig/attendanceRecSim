package ganzlustig.attendancerecsim

import java.time.Duration
import java.time.LocalDateTime

data class WorkShift(
    val from: LocalDateTime,
    val to: LocalDateTime)
{
    val duration : Duration = Duration.between(from, to)

    constructor(from: LocalDateTime, duration : Duration)
            : this(from, from.plus(duration))
}
