// SPDX-License-Identifier: GPL-3.0-or-later

package ganzlustig.attendancerecsim

import com.charleskorn.kaml.Yaml
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import com.github.doyaaaaaken.kotlincsv.dsl.csvWriter
import kotlinx.serialization.*
import kotlinx.serialization.internal.StringDescriptor
import net.fortuna.ical4j.model.DateTime
import org.slf4j.LoggerFactory
import java.io.File
import java.lang.reflect.Modifier
import java.time.*
import java.time.format.*
import java.time.temporal.ChronoField
import java.time.temporal.ChronoUnit
import java.util.*
import kotlin.math.round

val log = LoggerFactory.getLogger("ganzlustig.attendancerecsim.Util")

val LOCAL_DATE_FORMATTER = arrayOf(
    DateTimeFormatter.ISO_LOCAL_DATE,
    DateTimeFormatterBuilder()
        .appendValue(ChronoField.DAY_OF_MONTH, 2)
        .appendLiteral('.')
        .appendValue(ChronoField.MONTH_OF_YEAR, 2)
        .appendLiteral('.')
        .appendValue(ChronoField.YEAR, 2, 10, SignStyle.EXCEEDS_PAD)
        .toFormatter(),
    DateTimeFormatterBuilder()
        .appendValue(ChronoField.DAY_OF_MONTH, 2)
        .appendLiteral('.')
        .appendValue(ChronoField.MONTH_OF_YEAR, 2)
        .appendLiteral('.')
        .appendValue(ChronoField.YEAR, 4, 10, SignStyle.EXCEEDS_PAD)
        .toFormatter()
)

@Serializer(forClass=LocalDate::class)
object LocalDateSerializer {

    override val descriptor: SerialDescriptor =
        PrimitiveDescriptorWithName("LocalDate", StringDescriptor)

    override fun serialize(encoder: Encoder, obj: LocalDate) {
        encoder.encodeString(obj.toString())
    }

    override fun deserialize(decoder: Decoder): LocalDate {

        val str = decoder.decodeString()
        return LocalDate.parse(str)
    }
}

@Serializer(forClass=LocalTime::class)
object LocalTimeSerializer {

    override val descriptor: SerialDescriptor =
        PrimitiveDescriptorWithName("LocalTime", StringDescriptor)

    override fun serialize(encoder: Encoder, obj: LocalTime) {
        encoder.encodeString(obj.toString())
    }

    override fun deserialize(decoder: Decoder): LocalTime {

        val str = decoder.decodeString()
        return LocalTime.parse(str)
    }
}

fun toLocalDate(cal: Calendar) : LocalDate {
    return LocalDateTime.ofInstant(cal.toInstant(), cal.timeZone.toZoneId()).toLocalDate()
}

fun LocalDateTime.toDateTime(timeZone: ZoneId): DateTime {
    return DateTime(DateTime.from(this.atZone(timeZone).toInstant()))
}

fun LocalDate.at(hour: Int, minute: Int) : LocalDateTime {
    return LocalDateTime.of(this, LocalTime.of(hour, minute))
}

fun writeICal(simRes: SimulationResults, iCal: File) {

    val workCal = Simulation.convertToCalendar(simRes)
    log.info("Writing iCalendar file ${iCal.absolutePath} ...")
    iCal.parentFile.mkdirs()
    iCal.printWriter().use { out ->
        out.println(workCal)
    }
}

fun durationFromHours(hours: Double) : Duration {
    return Duration.of((hours * 60 * 60).toLong(), ChronoUnit.SECONDS)
}

fun Duration.minutesPart() : Int {
    return (seconds % 3600 / 60).toInt()
}

fun Duration.hoursPart() : Int {
    return (seconds / 3600).toInt()
}

fun Duration.secondsPart() : Int {
    return (seconds % 60).toInt()
}

fun periodFromHumanString(str: String) : Period {

    var parts = str.split('y')
    var i = 0
    val years = if (parts.size > 1) { parts[i++].toInt() } else 0

    parts = parts[i].split('m')
    i = 0
    val months = if (parts.size > 1) { parts[i++].toInt() } else 0

    parts = parts[i].split('d')
    i = 0
    val days = if (parts.size > 1) { parts[i].toInt() } else 0

    return Period.of(years, months, days)
}

fun Period.toHumanString() : String {

    val str = StringBuilder()
    if (years > 0) {
        str.append(years).append('y')
    }
    if (months > 0) {
        str.append(months).append('m')
    }
    if (days > 0) {
        str.append(days).append('d')
    }
    return str.toString()
}

fun durationFromHumanString(str: String) : Duration {

    var parts = str.split('h')
    var i = 0
    val hours = if (parts.size > 1) { parts[i++].toInt() } else 0

    parts = parts[i].split('m')
    i = 0
    val minutes = if (parts.size > 1) { parts[i++].toInt() } else 0

    parts = parts[i].split('s')
    i = 0
    val seconds = if (parts.size > 1) { parts[i].toInt() } else 0

    return Duration.ofSeconds(((60L * hours) + minutes) * 60 + seconds)
}

fun Duration.toHumanString() : String {

    val str = StringBuilder()
    str.append(hoursPart()).append('h')
    if (minutesPart() > 0) {
        str.append(minutesPart()).append('m')
    }
    if (secondsPart() > 0) {
        str.append(secondsPart()).append('s')
    }
    return str.toString()
}

fun LocalDate.toCalendarStart() : Calendar {

    val cal = Calendar.getInstance()
    cal[Calendar.YEAR] = year
    cal[Calendar.MONTH] = monthValue
    cal[Calendar.DAY_OF_MONTH] = dayOfMonth
    cal[Calendar.HOUR_OF_DAY] = 0
    cal.clear(Calendar.MINUTE)
    cal.clear(Calendar.SECOND)

    return cal
}

fun LocalDate.toCalendarEnd() : Calendar {

    val cal = Calendar.getInstance()
    cal[Calendar.YEAR] = year
    cal[Calendar.MONTH] = monthValue
    cal[Calendar.DAY_OF_MONTH] = dayOfMonth
    cal[Calendar.HOUR_OF_DAY] = 23
    cal[Calendar.MINUTE] = 59
    cal[Calendar.SECOND] = 59

    return cal
}

fun writeWorkShiftsCsv(simulationResults: SimulationResults, csv: File) {

    log.info("Writing work-shifts CSV file ${csv.absolutePath} ...")
    csv.absoluteFile.parentFile.mkdirs()
    csvWriter().open(csv) {
        writeRow(listOf("Name", "Date", "Start", "End", "Duration [s]"))
        for ((w, simRes) in simulationResults.workerResults) {
            val name = "${w.firstName} ${w.lastName}"
            for (t in simRes.workShifts) {
                writeRow(
                    listOf(
                        name,
                        t.from.toLocalDate().toString(),
                        t.from.toString(),
                        t.to.toString(),
                        t.duration.seconds
                    )
                )
            }
        }
    }
}

fun writeDayStatesCsv(simulationResults: SimulationResults, csv: File) {

    log.info("Writing day-states CSV file ${csv.absolutePath} ...")
    csv.absoluteFile.parentFile.mkdirs()
    csvWriter().open(csv) {
        val allStates = DayState.values()
        val headers = mutableListOf("Name", "Date")
        headers.addAll(allStates.map { it.name })
        writeRow(headers)
        for ((w, simRes) in simulationResults.workerResults) {
            val name = "${w.firstName} ${w.lastName}"
            for (ds in simRes.dayStates) {
                val row = mutableListOf(name, ds.key)
                row.addAll(allStates.map {
                    if (ds.value.contains(it)) {
                        "1"
                    } else {
                        "0"
                    }
                })
                writeRow(row)
            }
        }
    }
}

fun readWorkersCsv(workersFile: File) : List<Worker> {

    val workers = mutableListOf<Worker>()

    csvReader().open(workersFile) {

        val lines = readAllAsSequence().iterator()
        if (lines.hasNext()) {
            val firstLine = lines.next()
            var titleToIndex = Worker.parseHeader(firstLine)
            if (titleToIndex.isEmpty()) {
                // no header title matched with any Worker field
                // -> assuming there is no header row,
                //    and that the columns appear in the order
                //    of the fields/properties in class Worker
                titleToIndex = Worker.createDefaultHeader(firstLine.size)
                workers.add(Worker.parse(firstLine, titleToIndex))
            }
            lines.forEach { row ->
                workers.add(Worker.parse(row, titleToIndex))
            }
        }
    }

    return workers
}

fun writeWorkersCsv(workersFile: File, workers: List<Worker>) {

    csvWriter().open(workersFile) {
        writeRow(Worker::class.java.declaredFields.filter { !Modifier.isStatic(it.modifiers) }.map { it.name })
        workers.forEach {
            writeRow(it.serialize())
        }
    }
}

fun readWorkersYaml(workersFile: File) : List<Worker> {

    val yaml = workersFile.readText()
    return Yaml.default.parse(Worker.serializer().list, yaml)
}

fun writeWorkersYaml(workersFile: File, workers: List<Worker>) {

    val yaml = Yaml.default.stringify(Worker.serializer().list, workers)
    workersFile.writeText(yaml)
}

fun String.splitWithSpace(delimiter: String) : List<String> {
    return split(delimiter).map { it.trim() }
}

fun List<String>.foldWithDelim(delimiter: String) : String {
    return fold("", { initial, elem -> "${initial}${delimiter}$elem" }).substring(1)
}

fun fraction2Percent(fraction: Double) : Double {
    return round(fraction * 10000) / 100
}

fun Random.nextBoolean(likelihood: Double) : Boolean {

    var choice = nextDouble()
    val flip = nextBoolean()
    if (flip) {
        choice = 1.0 - choice
    }
    return choice < likelihood
}


/**
 * Supports dot (0.25) and comma (0,25) as separator.
 */
fun toDoubleLax(possibleDouble: String) : Double {

    return try {
        possibleDouble.toDouble()
    } catch (ex: java.lang.NumberFormatException) {
        possibleDouble.replace(',', '.').toDouble()
    }
}

fun toLocalDate(possibleDate: String) : LocalDate {

    // all but last from LOCAL_DATE_FORMATTER
    for (localDateFormatter in LOCAL_DATE_FORMATTER.slice(0..LOCAL_DATE_FORMATTER.size-2)) {
        try {
            return LocalDate.parse(possibleDate, localDateFormatter)
        } catch (ex: DateTimeParseException) {
            log.trace(ex.message)
        }
    }
    // last LOCAL_DATE_FORMATTER
    return LocalDate.parse(possibleDate, LOCAL_DATE_FORMATTER.last())
}
