package ganzlustig.attendancerecsim

import net.fortuna.ical4j.data.CalendarBuilder
import net.fortuna.ical4j.filter.Filter
import net.fortuna.ical4j.filter.PeriodRule
import net.fortuna.ical4j.model.*
import net.fortuna.ical4j.model.component.CalendarComponent
import net.fortuna.ical4j.model.property.Location
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.Calendar
import java.util.function.Predicate

@Suppress("SpellCheckingInspection")
const val HOLIDAYS_RES = "FeiertageDeutschland2010Bis2035.ics"
@Suppress("SpellCheckingInspection")
const val ALL_GERMAN_STATES = "Alle Bundesländer"
const val WORK_WEEK_HOURS = 42.0
const val WORK_DAY_HOURS = WORK_WEEK_HOURS / 5

// TODO Make this configurable
val DEFAULT_CORE_TIMES = listOf(
    WorkShiftPlan(DayOfWeek.MONDAY,     8, 0, 4),
    WorkShiftPlan(DayOfWeek.MONDAY,    13, 0, 5),
    WorkShiftPlan(DayOfWeek.TUESDAY,    8, 0, 4),
    WorkShiftPlan(DayOfWeek.TUESDAY,   13, 0, 5),
    WorkShiftPlan(DayOfWeek.WEDNESDAY,  8, 0, 4),
    WorkShiftPlan(DayOfWeek.WEDNESDAY, 13, 0, 5),
    WorkShiftPlan(DayOfWeek.THURSDAY,   8, 0, 4),
    WorkShiftPlan(DayOfWeek.THURSDAY,  13, 0, 5),
    WorkShiftPlan(DayOfWeek.FRIDAY,     8, 0, 4),
    WorkShiftPlan(DayOfWeek.FRIDAY,    13, 0, 2)
)
const val SHIFT_LIKELIHOOD_CORE = 0.95
const val SHIFT_LIKELIHOOD_DEFAULT = 0.05

data class DatePeriodInfo(
    val start : LocalDate,
    val end : LocalDate,
    val germanState: String = Worker.DEFAULT_GERMAN_STATE)
{
    private val startTime = startTime(start)
    private val endTime = endTime(end)
    val numDays = ChronoUnit.DAYS.between(start, end).toInt() + 1
    val numWeekDays = calculateWeekDays(start, end)
    val numNonWeekEndHolidays : Int
    val nonWeekEndHolidays : MutableList<LocalDate>
    val yearFraction = numDays.toDouble() / 365

    constructor(
        year: Int,
        germanState: String = Worker.DEFAULT_GERMAN_STATE)
            : this(startDate(year), endDate(year), germanState)

    init {
        val holidaysCalendar = localHolidaysInPeriod(start, end, germanState)

        // Create the years date range
        val period = Period(startTime, endTime)

        var numHolidays = 0
        var numNonWeekEndHolidaysTmp = 0
        val nonWeekEndHolidaysTmp = mutableListOf<LocalDate>()
        for (calComp in holidaysCalendar.components) {
            val events = calComp.calculateRecurrenceSet(period)
            for (event in events) {
                numHolidays++
                val c = Calendar.getInstance()
                c.time = event.start
                val dayOfWeek = c[Calendar.DAY_OF_WEEK]
                if (dayOfWeek != Calendar.SATURDAY && dayOfWeek != Calendar.SUNDAY) {
                    nonWeekEndHolidaysTmp.add(toLocalDate(c))
                    numNonWeekEndHolidaysTmp++
                }
            }
        }
        numNonWeekEndHolidays = numNonWeekEndHolidaysTmp
        nonWeekEndHolidays = nonWeekEndHolidaysTmp
    }

    companion object {
        fun startDate(year: Int) : LocalDate {
            return LocalDate.of(year, 1, 1)
        }

        fun endDate(year: Int) : LocalDate {
            return LocalDate.of(year, 12, 31)
        }

        fun startTime(date: LocalDate) : DateTime {
            return DateTime("${date.format(DateTimeFormatter.BASIC_ISO_DATE)}T000000Z") // TODO Do we need the Z?
        }

        fun endTime(date: LocalDate) : DateTime {
            return DateTime("${date.format(DateTimeFormatter.BASIC_ISO_DATE)}T235959Z") // TODO Do we need the Z?
        }

        private fun calculateWeekDays(start: LocalDate, end: LocalDate) : Int {

            var numWeekDays = 0

            var localDate: LocalDate = start
            while (localDate.isBefore(end)) {
                when (localDate.dayOfWeek) {
                    DayOfWeek.MONDAY -> numWeekDays++
                    DayOfWeek.TUESDAY -> numWeekDays++
                    DayOfWeek.WEDNESDAY -> numWeekDays++
                    DayOfWeek.THURSDAY -> numWeekDays++
                    DayOfWeek.FRIDAY -> numWeekDays++
                    else -> {
                    }
                }
                localDate = localDate.plusDays(1)
            }

            return numWeekDays
        }

        private fun createPeriodRule(from: LocalDate, to: LocalDate): Predicate<CalendarComponent?> {

            val yearBegin = from.toCalendarStart()
            val yearEnd = to.toCalendarEnd()

            val period = Period(DateTime(yearBegin.time), DateTime(yearEnd.time))
            return PeriodRule(period)
        }

        private fun createGermanStateRule(germanState: String): Predicate<CalendarComponent?> {

            return Predicate {
                val loc = it?.getProperty<Location>(Property.LOCATION)
                var matches = false
                loc?.value?.let { value: String -> if (value == ALL_GERMAN_STATES) matches = true }
                loc?.value?.split(", ")?.contains(germanState)?.let {
                        containsGermanState: Boolean -> if (containsGermanState) matches = true
                }
                matches
            }
        }

        /**
         * @param germanState For example "BE" for Berlin, or "BY" for Bavaria.
         */
        fun localHolidaysInPeriod(from: LocalDate, to: LocalDate, germanState: String): net.fortuna.ical4j.model.Calendar {

            // load the holidays from iCAL file,
            // downloaded from https://www.schulferien.eu/berlin/feiertage-be/
            val holidaysFileICal =
                this::class.java.classLoader.getResourceAsStream(HOLIDAYS_RES)
            val builder = CalendarBuilder()
            val calendar = builder.build(holidaysFileICal)

            val calComps: Collection<CalendarComponent> =
                calendar.getComponents(Component.VEVENT)
            val filteredComps = Filter(
                arrayOf(
                    createPeriodRule(from, to),
                    createGermanStateRule(germanState)
                ),
                Filter.MATCH_ALL)
                .filter(calComps)
            return Calendar(ComponentList(filteredComps.toList()))
        }
    }
}
