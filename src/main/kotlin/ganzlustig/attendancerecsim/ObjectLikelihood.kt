package ganzlustig.attendancerecsim

data class ObjectLikelihood<T>(val obj: T, val likelihood: Double)
